// Mathematical Operations (-, *, /, %)//
//Subtraction
let numString1 = "5";
let numString2 = "6";
let num1= 5;
let num2= 6;
let num3 = 5.5;
let num4 = .5;

console.log(num1-num3); 
//-0.5

console.log(num3-num4);
//5

console.log(numString1-num2);
// -1; String was forced to become a number

console.log(numString2- num2);
//0

console.log(numString1-numString2);
/*In subtraction, numeric strings will not concatenate. And
instead will be focibly changed its type and 
subtract properly
*/

let sample2 = "Juan Dela Cruz";
console.log(sample2 - numString2);
/*
NaN - Not a number.

When trying to perform subtraction between alpha numeric string
and numeric string, the resultis NaN.
*/

//Multiplication
console.log(num1 * num2);
//30

console.log(numString1*num1);
//25

console.log(numString1 *numString2);
//30

console.log(sample2 * 5);
//NaN

let product = num1 * num2;
let product2 = numString1 * num1;
let product3 = numString1 * numString2;
console.log	(product);
//30


//Division

console.log(product/num2);
//5

console.log(product2/5); 
//5

console.log(numString2 / numString1);
//1.2

console.log(numString2 % numString1);
//1

console.log(product2 * 0);
//0
/* Division by 0 is not accurately and should not 
be done. It results to infinity.
*/

// Modulo % - remainder of division operation.

console.log(product2 % num2);
// 25/6 = remainder 1

console.log(product3  % product2);
//30/25 = remainder 5

console.log(product3  % product3);
//0


//Boolean (t or f)

/*
Boolean is usually used for logic operaitions or 
if-else condition

-Boolean values are normally used to
store values relating to the state of certain things. 

When creating a varibale which will contain a boolean,
the variable name is usually a yes or no.

*/

let isAdmin = true;
let isMarried = false;
let isMVP = true;

// u cam also conacatenate strings + bool

console.log("is she is Married?: " + isMarried);
console.log("is he the MVP?: " + isMVP)
console.log("is he the current admin?: " + isAdmin);

//Arrays

/*

-arrays are a special kind of data type to store
multiple values.

-can actually store data with different types BUT
as as the best prracticce, arrays ar used to contail multple 
values of the same data type

-values in an arrays are separated by commas.

-An array is created with an array literal."[]"
*/
//Syntax: 
	// let/const arrayName = [element1, element2, ...];


let array1 = ["Goku", "Piccolo", "Gohan","Vegeta"];

console.log(array1);

let array2 = ["One punch man", true, 500, "Saitama"];
console.log(array2)

// arrays are better thought of as group of data

let grades = [98.7, 92.1, 90.2, 94.6];
console.log(grades);

//Objects
/*

Objects
	- are another special kind of data type used to mimic 
	the real world.

	- used to create comple data that containe pieces of info
	that are relevant to each other.

	-objects are created with object literals "{}"
		-each data/ value are parired with a key/
		-each field is called a property.
		-each field is separated comma.

	Syntax:
	let/const objectName = {
		
		propertyA: Value,
		propertyB: Value

	}

*/

let person = {

	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["+63991545423", "+6389842104"],
	address: 
	{
		houseNumber: "345",
		street: "Diamond",
		city: "Manila",
	}
}
console.log(person);

//MINI ACTIVITY

let queen = ["Brian May", "Roger Taylor", "Freddie Mercury",
"John Deacon"];

let description = {
first_name: "John Dave",
last_name: "Umban",
isDeveloper:false,
hasPortfolio: false,
age:19,
contact:"+639669383496",
address: {
	houseNumber:"420",
	street:"Canary Street",
	city:"Imus",
		  }
};
console.log(description);

/*
Undefined vs Null

	//Null - is explicit abscence of data/value.
	This is done to project that a variable contains
	nothing over undefined as undefined merely means
	there is no data in the variable because the variable 
	has not been assigned an initial value.

*/

let sampleNull = null;
console.log	(sampleNull);

//example:

let myNumber =0;
let myString ="";

//using null to compare to a 0 or an empty string is much
//better for readabiility

/*
// for undefined, this is normally caused by developers creating variables 
that have no value or data/associated with them.
*/

/* Undefined is a representation that a variable that has been
declared but it was not assigned an initial value*/

let sampleUndefined;
console.log(sampleUndefined);

let foundResult = null;

/* for undefined, this is normally caused by developers
creating varibales that have no value or
data/associated with them.
*/

/*This is when a variable does not exist but its value is 
still uknown*/


let person2 ={
	age: 35,
	name: 'Peter',
}

console.log(person2.isAdmin);

//undefined because person2 does exits but the 
//property isAdmin does not exist 


/*
[SELECTION] Functions
	Functions
		- in JavaScript are line/blocks of codes
		taht tell our device/appliction to perform
		a certain task when called.
		-reusable pieces of code with instructions
		which is used over and over. Just as long
		as we can call them.

Syntax:
function functionName(){
	code block
		- the block of code that will be exceuted
		once the function has been run or called
}


*/

function print_name1(){
	console.log("My name is John");
};

// calling of funciton - functionName();
print_name1();

function showSum(){

	console.log(25+6);
};

showSum();

function showSum(){

	console.log(5+6);
};

showSum();
// overwrite the before code to the latest


/*Parameters and Arguments
	"name" is called a parameter
		a parameter acts a named a variable/containter
		that exists only inside of the function. This is
		used as to store info to act as a stand-in or the 
		container the valued passeed into the function as an 
		argument.
*/

function print_name (name){

	console.log('My name is ' + name);
}

/*when a function is called/invoked and data is paased, 
we call the data as an argument*/

/* in this invocationn "Jin" is an argument pass in our
print_name functin and is representaed by the "name"
parameter within our function*/

/*data passed into the function: Argument
representation of the argument within the funcion: Parameter
*/
print_name("jin");


function displayNum (number){
	alert(number);
};
displayNum(4000)

// MINI ACTIVITY

function message(sample_message){

	console.log(`${sample_message} is fun!`);
}

message("JavaScript");

//multiple parameters and arguments

function full_name(firstname, mi, lastname, age){
	console.log(`${firstname} ${mi} ${lastname} ${age}`)
};

full_name("john dave", "j", "Umban", "19");

//return

/*
the return statement allows the output of a function to be passed to the
line/block of code that called/invoked the function.
*/

/*
any line/block of code that comes after the return statement is 
ignored because it ends the function execution.
*/

function createFullName(firstname, middlename, lastname){

	//return keyword is used so taht a function may return a value

	/*also stops the process of the function any other instruction
	after the keyword will not be preocessed*/

	// return firstname+ middlename+ lastname;
	console.log("I will no longer run")
	return firstname+ middlename+ lastname;
}

let fullname1 = createFullName('john', 'dave', 'umban');
let fullName3 = createFullName("taehyung", "cool", "gaming");
console.log(fullname1);
console.log(fullName3);